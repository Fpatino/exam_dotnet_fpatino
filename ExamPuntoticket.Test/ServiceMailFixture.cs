﻿using System.Collections.Generic;
using System.IO;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ExamPuntoticket.BLL.ServicesMail;

namespace ExamPuntoticket.Test
{
    [TestClass]
    public class ServiceMailFixture
    {
        [TestMethod]
        public void WhenSendMailLogFiles()
        {
            var mockILog = new Mock<ILog>();
            var sendMail = new ServiceMail(mockILog.Object);

            sendMail.SendMail(new List<FileInfo>{new FileInfo("test.txt")});
            mockILog.Verify(x => x.Info(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void WhenSendMailWithoutFilesThenEmptyLog()
        {
            var mockILog = new Mock<ILog>();
            var sendMail = new ServiceMail(mockILog.Object);

            sendMail.SendMail(new List<FileInfo>());
            mockILog.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
        }

        [TestMethod]
        public void WhenSendMailWithNullFileInfoThenEmptyLog()
        {
            var mockILog = new Mock<ILog>();
            var sendMail = new ServiceMail(mockILog.Object);

            sendMail.SendMail(null);
            mockILog.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
        }
    }
}