﻿using System.Collections.Generic;
using System.IO;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ExamPuntoticket.BLL.BusinessImpl.Process;
using ExamPuntoticket.BLL.ServicesMail;

namespace ExamPuntoticket.Test
{
    [TestClass]
    public class Class2Fixture
    {
        [TestMethod]
        public void WhenExecuteCallSendMail()
        {
            var mockSendMail = new Mock<IServiceMail>();
            var mockILog = new Mock<ILog>();

            var sendMailJob = new LoadProcessFileSvc(mockSendMail.Object, mockILog.Object);
            sendMailJob.Execute(null);

            mockSendMail.Verify(x => x.SendMail(It.IsAny<IEnumerable<FileInfo>>()), Times.Once);
        }
    }
}