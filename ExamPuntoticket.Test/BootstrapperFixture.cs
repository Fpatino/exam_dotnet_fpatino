﻿using ExamPuntoticket.BLL.BusinessImpl.Process;
using ExamPuntoticket.BLL.ServicesMail;
using ExamPuntoticket.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace ExamPuntoticket.Test
{
    [TestClass]
    public class BootstrapperFixture
    {
        [TestInitialize]
        public void SetUp()
        {
            Bootstrapper.Init();
        }

        [TestMethod]
        public void TestConfig()
        {
            Assert.IsInstanceOfType(Bootstrapper.Kernel.Get<IServiceMail>(), typeof(ServiceMail));
            Assert.IsInstanceOfType(Bootstrapper.Kernel.Get<ProcessFileSvc>(), typeof(ProcessFileSvc));
            Assert.IsInstanceOfType(Bootstrapper.Kernel.Get<LoadProcessFileSvc>(), typeof(LoadProcessFileSvc));
        }
    }
}
