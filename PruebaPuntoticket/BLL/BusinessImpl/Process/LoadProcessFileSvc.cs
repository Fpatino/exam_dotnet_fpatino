﻿using System.IO;
using log4net;
using Quartz;
using ExamPuntoticket.BLL.ServicesMail;

namespace ExamPuntoticket.BLL.BusinessImpl.Process
{
    public class LoadProcessFileSvc : IJob
    {
        private readonly ILog _log;
        private readonly IServiceMail _serviceMail;

        public LoadProcessFileSvc(IServiceMail serviceMail, ILog log)
        {
            _log = log;
            _serviceMail = serviceMail;
        }

        public void Execute(IJobExecutionContext context)
        {
            _log.Debug("Load File");
            var d = new DirectoryInfo(Directory.GetCurrentDirectory());
            var f = d.GetFiles("*.txt");

            _serviceMail.SendMail(f);

            _log.Debug("End Send Mail");
        }
    }
}