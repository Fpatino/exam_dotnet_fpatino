﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using log4net;
using Quartz;
using System.Configuration;
using ExamPuntoticket.BLL.DataTransferObject.Ticket;

namespace ExamPuntoticket.BLL.BusinessImpl.Process
{
    public class ProcessFileSvc : IJob
    {
        private readonly ILog _log;
        private const string archive = "code.txt";

        public ProcessFileSvc(ILog log)
        {
            _log = log;
        }

        public void Execute(IJobExecutionContext context)
        {
            _log.Debug("Start process file");

            //OBTENEMOS EL ARCHIVO Y AGREGAMOS LA LISTA A UN DTO.
            var l = File.ReadAllLines(Path.Combine(Directory.GetCurrentDirectory(), archive));
            List<TicketDto> lstTickets = l.Select(t => new TicketDto { Code = t.Split(',')[0], Type = t.Split(',')[1] }).ToList();

            /* 
             * OBTENEMOS LOS TIPOS DE CODIGOS QUE VIENEN EN EL ARCHIVO CODE.TXT, EN EL CASO DE AGREGAR UN NUEVO TIPO, 
             * SOLO SE DEBE AGREGAR EL APP.CONFIG, SIN LA NECESIDAD DE AGREGAR EL TIPO Y RECOMPILAR.
            */
            var lstNotEnumerated = ConfigurationManager.AppSettings["TypeNotEnumerated"].Split(';').ToList();
            var lstEnumerated = ConfigurationManager.AppSettings["TypeEnumerated"].Split(';').ToList();

            //OBTENEMOS LA CANTIDAD DE REGISTROS.
            _log.Debug("Read code types");
            var numerated = lstTickets.Where(t => lstEnumerated.Contains(t.Type)).Select(t => t.Code).ToList();
            var notNumerated = lstTickets.Where(t => lstNotEnumerated.Contains(t.Type)).Select(t => t.Code).ToList();
            var typeGroup = lstTickets.Where(t => t.Type != "Type").GroupBy(g => g.Type).Select(p => new TypeGroupDto { Type = p.Key, Count = p.Count() }).ToList();


            _log.Debug("Save file numerated");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "numerated_" + DateTime.Now.Ticks + ".txt")))
            {
                file.WriteLine(string.Join("\r\n", numerated));
            }

            _log.Debug("Save file not numerated");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "notnumerated_" + DateTime.Now.Ticks + ".txt")))
            {
                file.WriteLine(string.Join("\r\n", notNumerated));
            }

            _log.Debug("Save file group for types");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "typegroup_" + DateTime.Now.Ticks + ".txt")))
            {
                foreach (var line in typeGroup)
                {
                    file.WriteLine("[" + line.Type + ", " + line.Count.ToString() + "]");
                }
            }
        }
    }
}