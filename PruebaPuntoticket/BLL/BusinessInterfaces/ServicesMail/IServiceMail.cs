﻿using System.Collections.Generic;
using System.IO;

namespace ExamPuntoticket.BLL.ServicesMail
{
    public interface IServiceMail
    {
        void SendMail(IEnumerable<FileInfo> files);
    }
}