﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamPuntoticket.BLL.DataTransferObject.Ticket
{
    public class TicketDto
    {
        public string Code { get; set; }
        public string Type { get; set; }
    }
}
