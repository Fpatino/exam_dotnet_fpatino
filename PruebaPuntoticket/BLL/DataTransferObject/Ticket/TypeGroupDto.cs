﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamPuntoticket.BLL.DataTransferObject.Ticket
{
    public class TypeGroupDto
    {
        public string Type { get; set; }
        public int Count { get; set; }
    }
}
