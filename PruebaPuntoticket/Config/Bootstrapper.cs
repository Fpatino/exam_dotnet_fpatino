﻿using ExamPuntoticket.BLL.BusinessImpl.Process;
using ExamPuntoticket.BLL.ServicesMail;
using log4net;
using Ninject;
using Ninject.Modules;
using Quartz;
using System.Collections.Generic;

namespace ExamPuntoticket.Config
{
    public static class Bootstrapper
    {
        public static IKernel Kernel;
        public static void Init()
        {
            LogFactory.Configure();
            Kernel = new StandardKernel();

            Kernel.Bind<ILog>().ToMethod(x => LogManager.GetLogger("Log")).InSingletonScope();
            Kernel.Bind<IServiceMail>().To<ServiceMail>().InTransientScope();

            Kernel.Bind<IJob>().To<ProcessFileSvc>().InTransientScope().Named(typeof(ProcessFileSvc).Name);
            Kernel.Bind<IJob>().To<LoadProcessFileSvc>().InTransientScope().Named(typeof(LoadProcessFileSvc).Name);

            JobsRegister.Register();
        }
    }
}