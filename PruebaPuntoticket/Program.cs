﻿using ExamPuntoticket.Config;
using Ninject;
using Ninject.Modules;

namespace ExamPuntoticket
{
    class Program
    {
        static void Main(string[] args)
        {
            Bootstrapper.Init();
        }
    }
}
